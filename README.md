# Fyra i rad #

Du kan använda dig av klassen som finns i matrix.py. En klass är ett object med givna metoder och atributer förknippade. Den klassen som finns i matrix.py heter Matrix. Matrix är en spelplan som du kan använda dig av i ditt fyra i rad spel. För att skapa en ny spelplan skriver du:

```
#!python
Matrix(antal rader, antal kolumer)

```
Om du sedan väljer att använda print på ett Matrix-objekt så kommer det automatiskt att skapa ett rutnät på följande sätt och siffror åvanför som i exemplet nedan:
```
#!python
matris = Matrix(10, 10)
print(matris)
 1 2 3 4 5 6 7 8 9 10
_____________________
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
---------------------
```
Du kan även få ut ett index på en given ruta igenoma att skriva:
```
#!python
m = Matrix(6,7)
m[4][2]
' '
```
Den beter sig alltså nästan som en lista i en lista.
Notera att det är den nedre vänstra rutan som är 0, 0!

Matrix har även två metoder för att ta reda på hur många rader respektive kolumer en matris har. Dessa metoder heter getNumRows och getNumColumns. För att använda dessa skriver du:
```
#!python
matris = Matrix(6,7)

matris.getNumRows()
6
matris.getNumColumns()
7
```


Det du ska göra är att forka gitten så att du kan göra ditt egna fyra i rad spel. Du behöver skapa ett nytt dokument och sedan importera matrix.py. Därefter kan du använda dig av Matrix för att göra ditt fyra i rad spel.

Ett tips är att du börjar med att göra en funktion för att lägga brickor i given kolumn. Anledningen till att det är bra att börja med det är att det blir mycket lättare att testa övriga funktioner och för att då kan man rigga upp en bot som gör slumpmässiga drag. Det kan vara väldigt kul att spela mot en bot som bara lägger slumpmässigt, men det är inte bara för att ha kul som vi skapar en bot utan den ska användas senare.


## Extrauppgifter ##

Om du följde åvanstående instruktioner så har du gjort en bot som gör slumpmässiga drag. Denna bot kan du använda för att testa det som du ska göra nu. Din uppgift blir att göra en mer sofistikerad AI för att spela fyra-i-rad. Detta går att göra på många sätt men ett som kan rekomenderas är en så kallad min/max algoritm och det är den som kommer beskrivas här.

### Min/Max Algoritm ###

En så kallad Min/Max algoritm är en algoritm för att bestämma det bästa draget en spelare kan göra utifrån alla möjliga alternativ. Algoritmen bygger på att man genererar ett träd av alla möjliga spelplanar från det nuvarande läget. Sedan ger man en poäng till varje nod i trädet och på så sätt bedömer man vilket drag man ska utföra. Man vill maximera sitt egna värde medans man vill minimera motståndarens värde, därav namnet minimax.

### Generering av alla möjligheter ###
![A4ConnectFour-002.png](https://bitbucket.org/repo/Kz7d7a/images/2886185308-A4ConnectFour-002.png)

Det första som algoritmen måste göra är att generera alla möjliga dra och organisera de i ett träd. Åvan finns ett exempel på ett träd som är genererat till ett tic-tac-toe spel. Utgångspungten är den nuvarande spelplanen och därefte varje möjlig spelplan. Utifrån ett givet läge. Anledningen till att man vill spara detta i ett träd är för att varje nod sedan ska poängsättas på ett särskillt sätt vilket kräver att man känner till vilka spelplaner som kommer från vilka.

### Evaluering av alla spelplaner ###
![A4ConnectFour-004.png](https://bitbucket.org/repo/Kz7d7a/images/2180175555-A4ConnectFour-004.png)

När ett träd har genererats är det dags att börja evaluera spelplanerna, med andra ord ge ett numeriskt värde till var och en av de basserat på hur bra eller dåligt det är. Alla spelplaner som gynnar dig mer än din mostråndare får ett positivt värde medans spelplaner som gynnar din motståndare mer får ett negativt värde. För att kunna utmärka vinnande eller förlorande spelplaner är det smart att ge de positiv/negativ oändlighet (detta kan göras med `float('inf')`). När man sedan ger en poäng till varje nod så börjar man med att evaluera löven (de noder som är längst ifrån rot-noden) detta är helt upp till dig att göra, men saker som kan vara bra att tänka på är t.ex. att en spelplan där det finns tre i rad är bättre än en med enbart två i rad, alltså skulle den med tre i rad få en högre poäng en den med två i rad. När lövens poäng är färdig är det dags att evaluera alla andra noder. I bilden ovan är cirklarna dina drag och fyrkanterna motståndarens tur. När man ska avgöra vilken poäng en nod får måste man titta på vem det är som skulle göra draget. Är det din tur tar du dett största av barnens värden men om det är motståndarens tur ska du ta det minsta av barnens värden, därav namnet minimax algoritm. När man har gått igenom hela trädet kommer man till slut att komma fram till vilket drag som är bäst att utföra i det nuvarande läget.

Att generera ett träd och gå igenom hela trädet är en väldigt kostsam operation, alltså måste man begränsa antalet steg man kan gå. Antalet kan du själv experimentera med, det finns ingen direkt gräns till hur många man kan generera, det beror på hur bra dator man har och hur bra ens algoritm är. En annan sak som man kan experimentera med är poängsättningen till olika spelplaner för att på så sätt optimera sin AI.

Lycka till!